<?php
/**
 * @since 2022-02-16
 *   On a “503 Service Unavailable” error caching is set to 50 minutes
 *   (3000 seconds), identical to the caching of the main AMP v0.js script
 *   from cdn.ampproject.org.
 */
ob_start('ob_gzhandler');
http_response_code(503);
header('Allow: GET');
header('Cache-Control: private, max-age=3000');
header('Content-Type: text/html; charset=utf-8');
header('Retry-After: 3000');
header('X-Powered-By: StoreCore');
header('X-Robots-Tag: noindex', true);
require '503.html';
