<?php

declare(strict_types=1);

if (isset($_GET['f'])) {
    header('Cache-Control: public');
    header('HTTP/1.1 410 Gone');
    exit;
}

require 'includes/header.inc.php';
?>
<section class="amd-hero-image amd-hero-image--android-smartphone-on-black-background">
  <div amp-fx="parallax" data-parallax-factor="1.5" class="mdc-layout-grid">
    <div class="mdc-layout-grid__inner">
      <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-6-desktop" style="background-image:linear-gradient(to bottom, rgba(0,0,0,.54), rgba(0,0,0,.95));border-radius:10px;padding:8px">
        <h1 style="font-family:Roboto"><strong>Store</strong>Core</h1>
        <h2 class="mdc-typography--headline5" style="text-wrap:balance">Ecommerce in the palm of your hand</h2>
        <p style="font-size:18px;text-wrap:balance">Tired of broken software updates or a slow webshop?  Keep on reading!  We introduce to you <dfn>StoreCore</dfn>: the first open-source, fully mobile ecommerce platform built by and for online retailers.  We are building not only an <a class="amd-color--light-green-a700" href="#high-performance" title="High performance">extremely fast</a> ecommerce platform (loads pages in less than a second), but we also put <a class="amd-color--light-green-a700" href="#mobile-first" title="Mobile first">mobile first</a> &mdash;&nbsp;everywhere and anywhere.  Yes, from now on you can simply run your online store from the palm of your hand.</p>
        <p style="font-size:18px"><a class="amd-color--light-green-a700" href="#feature-highlights" style="text-decoration:none" title="Key feature highlights"><i aria-hidden="true" class="material-icons" style="display:inline-flex;padding:0 0 2px 0;vertical-align:middle">&#xE5C8;</i> Feature highlights</a></p>
      </div>
      <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-6-desktop">
        &nbsp;
      </div>
    </div>
  </div>
</section>

<section class="amd-hero-image--php-code-in-2k">
  <div class="mdc-layout-grid" style="background-image:linear-gradient(to right, rgba(0,0,0,.54), rgba(0,0,0,.95))">
    <div class="mdc-layout-grid__inner">
      <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-5-desktop">&nbsp;</div>
      <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-6-desktop">
        <h2>Sign up for a beta</h2>
        <p>Join the Early Adopter Program!</p>
        <p style="text-wrap:balance">We send out a limited number of beta invitations, based on several undisclosed criteria.
          If you're interested, sign up here.
          If selected, you'll receive an email with instructions on how to install the StoreCore client and provide feedback.
          Customer service cannot assist with beta access.</p>
        <p style="font-size:18px"><a class="amd-color--light-blue-a700" href="https://forms.gle/zyNkwELiG646iRsm9" style="text-decoration:none" title="StoreCore Early Adopter Program"><i aria-hidden="true" class="material-icons" style="display:inline-flex;padding:0 0 2px 0;vertical-align:middle">&#xE5C8;</i> Sign up today</a></p>
      </div>
      <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-1-desktop">&nbsp;</div>
    </div>
  </div>
</section>

<section class="amd-background-color--white" id="feature-highlights" style="content-visibility:auto;padding-top:48px">
  <h2 style="text-align:center">Key feature highlights</h2>
  <div class="mdc-layout-grid">
    <div class="mdc-layout-grid__inner">
      <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-3-desktop">
        <h3 class="mdc-typography--headline5" id="mobile-first"><i class="material-icons" role="presentation" style="color:#9c27b0">smartphone</i> Mobile first</h3>
        <p>Why not start with mobile?  Everyone is using their smartphones today for almost every&shy;thing.  Now you can use your mobile phone to run your business. Anyplace, anytime, and anywhere.</p>
      </div>
      <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-3-desktop">
        <h3 class="mdc-typography--headline5" id="high-performance"><i class="material-icons" role="presentation" style="color:#f44336">whatshot</i> High performance</h3>
        <p>We are doing everything to set our goals and standards high.  Performance is one of the main goals: your StoreCore webshop will be among the fastest sites in your industry.  StoreCore has a native full-page cache and supports Google <abbr title="Accelerated Mobile Pages">AMP</abbr> (Accelerated Mobile Pages).</p>
      </div>
      <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-3-desktop">
        <h3 class="mdc-typography--headline5"><i class="material-icons" role="presentation" style="color:#2196f3">security</i> Security by design</h3>
        <p>StoreCore is built with security in mind.  Store&shy;Core is <abbr title="Payment Card Industry Data Security Standard">PCI DSS</abbr> compliant and includes security features like whitelists, blacklists, and military-grade password encryption.  To emphasize our dedication to security, all <a href="/plans-and-pricing" title="StoreCore plans and pricing">StoreCore hosting plans</a> include a free <abbr title="Secure Sockets Layer">SSL</abbr> certificate.</p>
      </div>
      <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-3-desktop">
        <h3 class="mdc-typography--headline5"><i class="material-icons" role="presentation" style="color:#8bc34a">shopping_cart</i> Dedicated to commerce</h3>
        <p>StoreCore is not some overgrown blog tool or yet another <abbr title="content management system">CMS</abbr>.  It’s a dedicated ecom&shy;merce platform.  Built by, for, and with expe&shy;ri&shy;enced ecommerce and retail pro&shy;fes&shy;sion&shy;als.</p>
      </div>
      <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-3-desktop">
        <h3 class="mdc-typography--headline5"><i class="material-icons" role="presentation" style="color:#3f51b5">business</i> Multi-store support</h3>
        <p>StoreCore allows you to run hundreds of dif&shy;fer&shy;ent webshops selling millions of prod&shy;ucts.  StoreCore supports all world cur&shy;ren&shy;cies, international shipping, and <a href="https://www.storecore.io/knowledge-base/developer-guides/internationalization-and-localization" title="Internationalization and localization">multiple languages</a>.  Currently English, French, German, and Dutch are fully supported.</p>
      </div>
      <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-3-desktop">
        <h3 class="mdc-typography--headline5"><i class="material-icons" role="presentation" style="color:#4caf50">eco</i> Evergreen</h3>
        <p>Just like modern web browsers, StoreCore is an <dfn>evergreen</dfn> software framework: security patches and other software updates are downloaded and installed automatically.</p>
      </div>
      <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-3-desktop">
        <h3 class="mdc-typography--headline5"><i class="material-icons" role="presentation" style="color:#ff5722">widgets</i> Scalable</h3>
        <p>StoreCore is an open-source software framework for building webshops and online storefronts as well as ecommerce and online retail apps. StoreCore has a <abbr title="representational state transfer">REST</abbr> <abbr title="application programming interface">API</abbr> and can be used for headless commerce.</p>
      </div>
      <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-3-desktop">
        <h3 class="mdc-typography--headline5"><i class="material-icons" role="presentation" style="color:#795548">free_breakfast</i> Freedom</h3>
        <p>No more vendor lock-ins! StoreCore is <dfn>free and open-source software</dfn> (<abbr title="free and open-source software">FOSS</abbr>), published under version&nbsp;3 of the <a href="https://www.storecore.io/licenses" rel="license">GNU General Public License</a> (<abbr title="GNU General Public License version 3">GPLv3</abbr>). There is no proprietary or hidden software code: all open-source code is fully accessible and adjustable. And what’s more: all your business data is yours. Of&nbsp;course.</p>
      </div>
    </div>
  </div>
</section>
<?php require 'includes/footer.inc.php';
