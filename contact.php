<?php
define('AMP_FX_COLLECTION', true);
require 'includes/header.inc.php';
?>
<div class="mdc-layout-grid amd-background-color--white" style="padding-top:48px;padding-bottom:48px">
  <div class="mdc-layout-grid__inner">
    <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-2-desktop"><!-- (blank) --></div>
    <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-2-desktop">
      <address>
        <p class="mdc-typography--body1"><strong>StoreCore</strong>™<br>Sjaak Verbiezenlaan 7<br>5626&nbsp;HG&ensp;Eindhoven<br>The Netherlands</p>
      </address>
      <p><a class="amd-color--red-500" href="https://goo.gl/maps/4DrEdcec5kzmRGkSA" style="text-decoration:none" title="Google Maps"><i class="material-icons" style="display:inline-block;margin-bottom:8px;padding-top:2px;vertical-align:-25%;">location_on</i> Google Maps</a></p>
    </div>
    <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-2-desktop mdc-layout-grid__cell--align-middle">
      <div>
        <a href="/StoreCore.vcf" title="vCard">
          <amp-img alt="Scan this QR code for contact information." height="300" layout="fixed" src="/images/contact-qr-code.png" style="max-width:100%" width="300">
        </a>
      </div>
      <p class="mdc-typography--body1"><a href="/StoreCore.vcf" title="vCard">Download vCard</a><br><span class="mdc-typography--body2">or scan the QR code</span></p>
    </div>
    <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-3-desktop">
      <p class="mdc-typography--body1"><strong>Helpdesk Customer Support</strong><br><a href="https://storecore.freshdesk.com/support/home">storecore.freshdesk.com</a></p>
      <p class="mdc-typography--body1"><strong>E-mail</strong><br><a href="mailto:info@storecore.org" title="info@storecore.org">info@storecore.org</a></p>
      <p class="mdc-typography--body1"><strong>Phone</strong><br><a href="tel:+31-40-2482311" title="+31-40-248-2311">+31&nbsp;(40)&nbsp;248&#8239;23&#8239;11</a><br>Office hours 8:00 to 20:00 <abbr title="Central European Summer Time">CEST</abbr></p>
    </div>
    <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-3-desktop">
      <p class="mdc-typography--body1"><strong>Dutch Commercial Register</strong><br>23083293</p>
      <p class="mdc-typography--body1"><strong><abbr title="value-added tax">VAT</abbr> <abbr title="identification number">ID</abbr></strong><br>NL 001683200B54</p>
      <p class="mdc-typography--body1"><strong><abbr title="International Bank Account Number">IBAN</abbr> Bunq bank account</strong><br>NL19 BUNQ 2206 3036 20<br><abbr title="Business Identifier Code">BIC</abbr>/<abbr title="Society for Worldwide Interbank Financial Telecommunication">SWIFT</abbr> BUNQNL2A</p>
    </div>
  </div>
</div>

<!-- @todo Add more/other images. -->
<script type="application/ld+json">
{
  "@context": "https://schema.org/",
  "@type": "LocalBusiness",
  "@id": "https://www.storecore.io/",
  "name": "StoreCore",
  "legalName": "StoreCore",
  "alternateName": "StoreCore DevOps",
  "brand": {
    "@type": "Brand",
    "logo": "https://www.storecore.io/images/StoreCore-icon-196x196.png",
    "name": "StoreCore",
    "alternateName": "StoreCore.io",
    "sameAs": "https://www.storecore.io/" 
  },
  "image": "https://www.storecore.io/images/StoreCore-icon-216x216.png",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "Sjaak Verbiezenlaan 7",
    "postalCode": "5626 HG",
    "addressLocality": "Eindhoven",
    "addressCountry": "NL",
    "sameAs": "https://goo.gl/maps/4DrEdcec5kzmRGkSA"
  },
  "geo": {
    "@type": "GeoCoordinates",
    "latitude": 51.474686,
    "longitude": 5.4308338
  },
  "hasMap": "https://goo.gl/maps/6wWMvUcstp8F4CY3A",
  "url": "https://www.storecore.io/",
  "email": "info@storecore.org",
  "telephone": "+31402482311",
  "vatID": "NL 001683200B54",
  "ownershipFundingInfo": [
    "https://openkvk.nl/openkvk/hoofdvestiging-23083293-0000-storecore",
    "https://www.kvk.nl/orderstraat/bedrijf-kiezen/?q=23083293"
  ],
  "currenciesAccepted": "EUR",
  "priceRange": "€€€",
  "openingHoursSpecification": {
    "@type": "OpeningHoursSpecification",
    "dayOfWeek": [
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday"
    ],
    "opens": "08:00",
    "closes": "20:00",
    "validFrom": "2020-04-03"
  }
}
</script>

<div class="amd-background-color--ral-9010" >
  <amp-img alt="" amp-fx="fade-in" data-duration="2500ms" attribution="Photo by Marc Mueller from Pexels" height="540" layout="responsive" role="presentation" src="/images/380768-1920x540.webp" srcset="/images/380768-1366x384.webp 1366w, /images/380768-1920x540.webp 1920w, /images/380768-3840x1080.webp 3840w, /images/380768-5120x1440.webp 5120w" width="1920">
    <amp-img alt="" fallback height="540" layout="responsive" role="presentation" src="/images/380768-1920x540.jpg" srcset="/images/380768-1366x384.jpg 1366w, /images/380768-1920x540.jpg 1920w, /images/380768-3840x1080.jpg 3840w, /images/380768-5120x1440.jpg 5120w" width="1920">
  </amp-img>
</div>

<?php require 'includes/footer.inc.php';
