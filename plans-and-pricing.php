<?php require 'includes/header.inc.php'; ?>
<article>
  <h1>Plans and pricing</h1>
  <section>
    <h3 style="text-wrap:balance">The best value package for your online shops</h3>
    <div class="amd-data-table" style="display:block;overflow:auto;white-space:nowrap">
      <table aria-label="StoreCore plans and pricing">
        <thead>
          <tr>
            <th>&nbsp;</th>  
            <th role="columnheader" scope="col">StoreCore</th>
            <th role="columnheader" scope="col">StoreCore One</th>
            <th role="columnheader" scope="col">StoreCore Blue</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>&nbsp;</td>
            <td>Freemium</td>
            <td>Pay as you go</td>
            <td>All-inclusive</td>
          </tr>
          <tr>
            <td>Price</td>
            <td>Free download</td>
            <td>€ 89 per month</td>
            <td>€ 289 per month</td>
          </tr>
          <tr>
            <td>Installation</td>
            <td>·</td>
            <td class="amd-color--light-green-500">✔</td>
            <td class="amd-color--light-green-500">✔</td>
          </tr>
          <tr>
            <td>Data migration</td>
            <td>·</td>
            <td>·</td>
            <td class="amd-color--light-green-500">✔</td>
          </tr>
          <tr>
            <td>Dedicated hosting</td>
            <td>·</td>
            <td class="amd-color--light-green-500">✔</td>
            <td class="amd-color--light-green-500">✔</td>
          </tr>
          <tr>
            <td>Unlimited data traffic</td>
            <td>·</td>
            <td class="amd-color--light-green-500">✔</td>
            <td class="amd-color--light-green-500">✔</td>
          </tr>
          <tr>
            <td>Firewall and load balancers</td>
            <td>·</td>
            <td class="amd-color--light-green-500">✔</td>
            <td class="amd-color--light-green-500">✔</td>
          </tr>
          <tr>
            <td>Email addresses</td>
            <td>·</td>
            <td>5</td>
            <td>100</td>
          </tr>
          <tr>
            <td>Disk space</td>
            <td>·</td>
            <td>10 GB</td>
            <td>100 GB</td>
          </tr>
          <tr>
            <td>SSL certificate</td>
            <td>·</td>
            <td class="amd-color--light-green-500">✔</td>
            <td class="amd-color--light-green-700">✔</td>
          </tr>
          <tr>
            <td>Online documentation</td>
            <td class="amd-color--light-green-500">✔</td>
            <td class="amd-color--light-green-500">✔</td>
            <td class="amd-color--light-green-500">✔</td>
          </tr>
          <tr>
            <td>Email ticketing support</td>
            <td>€ 85 per hour</td>
            <td class="amd-color--light-green-700">free</td>
            <td class="amd-color--light-green-700">free</td>
          </tr>
          <tr>
            <td>Email, chat, and phone support</td>
            <td>€ 85 per hour</td>
            <td>€ 85 per hour</td>
            <td class="amd-color--light-green-700">free</td>
          </tr>
          <tr>
            <td>Manual database backups</td>
            <td class="amd-color--light-green-500">✔</td>
            <td class="amd-color--light-green-500">✔</td>
            <td class="amd-color--light-green-500">✔</td>
          </tr>
          <tr>
            <td>Daily database backups</td>
            <td>·</td>
            <td class="amd-color--light-green-500">✔</td>
            <td class="amd-color--light-green-500">✔</td>
          </tr>
          <tr>
            <td>Full database replication</td>
            <td>·</td>
            <td>·</td>
            <td class="amd-color--light-green-500">✔</td>
          </tr>
          <tr>
            <td>Google Analytics 4 (<abbr title="Google Analytics 4">GA4</abbr>)</td>
            <td class="amd-color--light-green-500">✔</td>
            <td class="amd-color--light-green-500">✔</td>
            <td class="amd-color--light-green-500">✔</td>
          </tr>
          </tr>
          <tr>
            <td>AMP experiments</td>
            <td>·</td>
            <td>·</td>
            <td class="amd-color--light-green-500">✔</td>
          </tr>
          <tr>
            <td>Monthly <abbr title="search engine optimization">SEO</abbr> health checkup</td>
            <td>·</td>
            <td>·</td>
            <td class="amd-color--light-green-500">✔</td>
          </tr>
          <tr>
            <td>Priority feature requests</td>
            <td>·</td>
            <td class="amd-color--light-green-500">✔</td>
            <td class="amd-color--light-green-700">✔</td>
          </tr>
        </tbody>
      </table>
    </div>
    <p class="mdc-typography--body2">Additional transaction fees, subscriptions, or other charges may apply to third-party software and services.
      <abbr title="value-added tax">VAT</abbr> is not included.</p>
  </section>

  <section itemscope itemtype="https://schema.org/FAQPage">
    <details itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
      <summary class="mdc-typography--headline6" itemprop="name">Is data migration possible?</summary>
      <span itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer">
        <span itemprop="text">
          StoreCore Blue includes free mi&shy;gra&shy;tion of product data, product cate&shy;gories, and product brands for up to 1&nbsp;million products.
          If your current data is sufficiently compatible with the StoreCore data&shy;base archi&shy;tec&shy;ture, we will also migrate up to 1&nbsp;million customer accounts and all customer groups.
          Because StoreCore employs very strong pass&shy;word encryp&shy;tion, your customers may have to reset their password once.
        </span>
      </span>
    </details>
    <details itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
      <summary class="mdc-typography--headline6" itemprop="name">Do I need an <abbr title="Secure Sockets Layer">SSL</abbr> certificate?</summary>
      <span itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer">
        <span itemprop="text">
          The safety of your webshops is our concern.
          StoreCore provides you with a free <abbr title="Secure Sockets Layer">SSL</abbr> (Secure Sockets Layer) certificate in all hosting plans.
          StoreCore One includes an <abbr title="Domain Validation">DV</abbr> (Domain Validation) <abbr title="Secure Sockets Layer">SSL</abbr> certificate for a single domain name; 
          StoreCore Blue includes a more extensive <abbr title="Extended Validation">EV</abbr> (Extended Validation) <abbr title="Secure Sockets Layer">SSL</abbr> certificate for multiple domains.
        </span>
      </span>
    </details>
    <details itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
      <summary class="mdc-typography--headline6" itemprop="name">Monthly or annual billing?</summary>
      <span itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer">
        <span itemprop="text">
          Domain names and the StoreCore One hosting plan are billed once per year.
          All other StoreCore plans, software products and services are billed per month.
        </span>
      </span>
      </details>
    <details itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
      <summary class="mdc-typography--headline6" itemprop="name">How can I cancel a subscription?</summary>
      <span itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer">
        <span itemprop="text">
          You may cancel any StoreCore hosting plan subscription with a 30&nbsp;day prior notice.
          Please contact Customer Care at <a href="mailto:info@storecore.org">info@storecore.org</a> or <a href="tel:+31-40-2482311" title="+31-40-248-2311">+31&nbsp;(40)&nbsp;248&#8239;23&#8239;11</a>.
          Upon cancellation, all your customer data, sales data, and product catalogue data remains yours and is accessible for easy transfer.
        </span>
      </span>
    </details>
  </section>
</article>

<!-- @todo Add `aggregateRating` and `offers`. -->
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "SoftwareApplication",
  "name": "StoreCore",
  "operatingSystem": "Linux",
  "applicationCategory": "https://schema.org/BusinessApplication",
  "applicationSubCategory": "https://schema.org/WebApplication"
}
</script>

<?php require 'includes/footer.inc.php';
