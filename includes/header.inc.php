<?php

declare(strict_types=1);

// Enable or disable PHP error reporting
if (isset($_GET['debug'])) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
} else {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(0);
}

// Set default timezone to CET (Central European Time)
date_default_timezone_set('Europe/Amsterdam');

// Enable compression
ob_start('ob_gzhandler');

// Response headers
header('Allow: GET, HEAD', true);
header('Cache-Control: public, immutable, max-age=86400', true);
header('Content-Language: en-GB', true);
header('Content-Security-Policy: upgrade-insecure-requests', true);
header('Content-Type: text/html; charset=utf-8', true);
header('Link: <https://cdn.ampproject.org/v0.js>; rel="preconnect"', true);
header('Link: <https://fonts.googleapis.com/>; rel="preconnect"', false);
header('Link: <https://fonts.gstatic.com/>; rel="preconnect"; crossorigin', false);
header('Permissions-Policy: fullscreen=(self), geolocation=(), microphone=()', true);
header('Referrer-Policy: same-origin', true);
header('Strict-Transport-Security: max-age=31536000; includeSubDomains', true);
header('X-Content-Type-Options: nosniff', true);
header('X-DNS-Prefetch-Control: on', true);
header('X-Frame-Options: SAMEORIGIN', true);
header('X-Robots-Tag: index, follow', true);
header('X-UA-Compatible: IE=edge', true);
header('X-XSS-Protection: 1; mode=block', true);

// HTTP request method and request URI are required
if (
    !isset($_SERVER['REQUEST_METHOD'])
    || !isset($_SERVER['REQUEST_URI'])
    || empty($_SERVER['REQUEST_URI'])
) {
    header('Internal Server Error', true, 500);
    ob_flush();
    flush();
    exit(1);
}

// Allow GET and HEAD requests.
$_SERVER['REQUEST_METHOD'] = strtoupper($_SERVER['REQUEST_METHOD']);
if ($_SERVER['REQUEST_METHOD'] === 'HEAD') {
    ob_flush();
    flush();
    exit;
} elseif ($_SERVER['REQUEST_METHOD'] !== 'GET') {
    header('Method Not Allowed', true, 405);
    ob_flush();
    flush();
    exit;
}

/**
 * Add page variables: `$canonical` contains the `storecore.io` canonical
 * URL, `$description` the text for the meta tag `description`, and `$title`
 * the page title.  The `$open_graph_image` is an optional URL for the
 * Open Graph `og:image` property.
 *
 * @see https://ogp.me/
 *      The Open Graph protocol
 *
 * @see https://developer.twitter.com/en/docs/tweets/optimize-with-cards/guides/getting-started
 *      Optimize Tweets with Cards - Twitter Developers
 * 
 * @see https://business.twitter.com/en/help/campaign-setup/advertiser-card-specifications.html
 *      Twitter ad specs and formats - Twitter Business
 *
 * @see https://blog.hubspot.com/marketing/open-graph-tags-facebook-twitter-linkedin
 *      How to Optimize Blog Images for Social Sharing: An Intro to Open Graph Tags, by Lindsay Kolowich
 */
$description = '';
$title = 'StoreCore';
$open_graph_image = 'https://www.storecore.io/images/331990-1200x675.jpg';

$_SERVER['REQUEST_URI'] = strip_tags($_SERVER['REQUEST_URI']);
$_SERVER['REQUEST_URI'] = mb_strtolower($_SERVER['REQUEST_URI'], 'UTF-8');
$_SERVER['REQUEST_URI'] = str_replace('.php', '', $_SERVER['REQUEST_URI']);

$uri = strtok($_SERVER['REQUEST_URI'], '?');
$canonical = 'https://www.storecore.io' . $uri;
switch ($uri) {
    case '/':
        $canonical   = 'https://www.storecore.io/';
        $description = 'StoreCore is an open-source ecommerce framework.';
        $title       = 'StoreCore™';
        break;
    case '/contact':
        $canonical        = 'https://www.storecore.io/contact';
        $description      = 'Office location, email address, telephone number, and other contact details for StoreCore in Eindhoven.';
        $open_graph_image = 'https://www.storecore.io/images/380768-1200x675.jpg';
        $title            = 'Contact StoreCore';
        break;
    case '/knowledge-base/':
        $canonical        = 'https://www.storecore.io/knowledge-base/';
        $description      = 'The StoreCore knowledge base (KB) contains manuals, guides, and instructions for users, designers, and developers.';
        $open_graph_image = 'https://www.storecore.io/images/knowledge-base-1200x675.jpg';
        $title            = 'Knowledge Base (KB)';
        break;
    case '/knowledge-base/design-guides':
    case '/knowledge-base/design-guides/':
        $canonical        = 'https://www.storecore.io/knowledge-base/design-guides/';
        $title            = 'StoreCore design guides';
        break;
    case '/knowledge-base/design-guides/storecore-brand-identity-guide':
        $canonical        = 'https://www.storecore.io/knowledge-base/design-guides/storecore-brand-identity-guide';
        $description      = 'This style guide provides guidelines to support your creative use and a consistent implementation of StoreCore’s corporate style and brand identity.';
        $open_graph_image = 'https://www.storecore.io/images/storecore-brand-identity-guide-1200x675.jpg';
        $title            = 'StoreCore brand identity guide';
        break;
    case '/knowledge-base/developer-guides':
    case '/knowledge-base/developer-guides/':
        $canonical        = 'https://www.storecore.io/knowledge-base/developer-guides/';
        $title            = 'StoreCore developer guides';  
        break;
    case '/knowledge-base/developer-guides/internationalization-and-localization':
        $canonical        = 'https://www.storecore.io/knowledge-base/developer-guides/internationalization-and-localization';
        $description      = 'StoreCore supports multiple webshops in multiple languages. This guide describes how internationalization (I18N) and localization (L13N) are implemented.';
        $open_graph_image = 'https://www.storecore.io/images/internationalization-and-localization-1200x675.jpg';
        $title            = 'Internationalization (I18N) and localization (L10N)';  
        break;
    case '/knowledge-base/user-guides':
    case '/knowledge-base/user-guides/':
        $canonical   = 'https://www.storecore.io/knowledge-base/user-guides/';
        $title       = 'StoreCore user guides';
        break;
    case '/knowledge-base/user-guides/installation-quickstart-guide':
        $canonical   = 'https://www.storecore.io/knowledge-base/user-guides/installation-quickstart-guide';
        $title       = 'Installation QuickStart Guide (QSG)';
        break;
    case '/license':
    case '/licenses':
        $canonical   = 'https://www.storecore.io/licenses';
        $description = 'StoreCore is available as free and open-source software (FOSS) under the GNU General Public License version 3 (GPLv3) and includes open-source software under a variety of other licenses.';
        $title       = 'Licenses';
        break;
    case '/privacy-policy':
        $canonical   = 'https://www.storecore.io/privacy-policy';
        $title       = 'Privacy policy';
        break;
    default:
        $canonical   = 'https://www.storecore.io/';
        $description = 'StoreCore is an open-source ecommerce framework.';
        $title       = 'StoreCore™';
}

// Add missing `description` to prevent empty metadata.
if (!isset($description) || empty($description)) {
    $description = $title;
}

// Append ' - StoreCore' to page title without 'StoreCore'.
if (strpos($title, 'StoreCore') === false) {
    $title .= ' - StoreCore';
}

// Convert special HTML characters
$canonical = htmlspecialchars($canonical);
$description = htmlspecialchars($description);
$title = htmlspecialchars($title);

if (!empty($_SERVER['REQUEST_TIME_FLOAT'])) {
    header('Server-Timing: total;dur=' . round(1000 * (microtime(true) - $_SERVER['REQUEST_TIME_FLOAT']), 1));
}
ob_flush();
flush();
?>
<!doctype html>
<html ⚡ dir="ltr" lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <link href="https://cdn.ampproject.org/v0.js" rel="preload" as="script">

    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="description" content="<?= $description ?>">
    <meta name="format-detection" content="telephone=no">
    <meta name="msapplication-TileColor" content="#dcedc8">
    <meta name="msapplication-TileImage" content="/images/StoreCore-icon-144x144.png">
    <meta name="theme-color" content="#689f38">

    <meta name="twitter:card" content="summary"> 
    <meta name="twitter:site" content="@storecoreio">
    <meta property="og:title" content="<?= $title ?>">
    <meta property="og:url" content="<?= $canonical ?>">
    <meta property="og:description" content="<?= $description ?>">
    <meta property="og:image" content="<?= $open_graph_image ?>">
    <meta property="og:site_name" content="StoreCore">

    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
    <script async custom-element="amp-fx-collection" src="https://cdn.ampproject.org/v0/amp-fx-collection-0.1.js"></script>
<?php if (\defined('AMP_YOUTUBE') && AMP_YOUTUBE === true): ?>
    <script async custom-element="amp-youtube" src="https://cdn.ampproject.org/v0/amp-youtube-0.1.js"></script>
<?php endif; ?>
    <script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>

    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@900&display=swap&text=Seort" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <style amp-custom><?php require 'storecore.min.css' ?></style>
    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>

    <link rel="canonical" href="<?= $canonical ?>">
    <title><?= $title ?></title>

    <link href="/images/StoreCore-icon-152x152.png" rel="apple-touch-icon-precomposed">
    <link href="/images/StoreCore-icon-196x196.png" rel="shortcut icon" sizes="196x196">
    <link href="/images/StoreCore-icon-152x152.png" rel="apple-touch-icon" sizes="152x152">
    <link href="/images/StoreCore-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">
    <link href="/images/StoreCore-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
    <link href="/images/StoreCore-icon-196x196.png" rel="icon" sizes="196x196">
    <link href="/images/StoreCore-icon-152x152.png" rel="icon" sizes="152x152">
    <link href="/images/StoreCore-icon-144x144.png" rel="icon" sizes="144x144">
    <link href="/images/StoreCore-icon-114x114.png" rel="icon" sizes="114x114">

    <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "Organization",
      "name": "StoreCore",
      "alternateName": "StoreCore.io",
      "legalName": "StoreCore",
      "url": "https://www.storecore.io/",
      "logo": "https://www.storecore.io/images/StoreCore-icon-196x196.png"
    }
    </script>
  </head>
  <body itemscope itemtype="https://schema.org/WebPage">
    <amp-sidebar id="sidebar" itemscope itemtype="https://schema.org/WPSideBar" layout="nodisplay" side="left">
      <div class="mdc-drawer__content" style="content-visibility:auto">
        <div class="mdc-drawer__header">
          <a href="https://www.storecore.io/" style="font-family:Roboto" title="StoreCore">
            <amp-img alt="StoreCore" height="48" layout="fixed" src="/images/StoreCore-icon-144x144.png" srcset="/images/StoreCore-icon-144x144.webp 144w, /images/StoreCore-icon-196x196.webp 196w" width="48"></amp-img>
            <strong>Store</strong>Core
          </a>
        </div>
        <nav role="menu">
          <ul role="group">
            <li role="presentation"><a <?php if ($uri === '/') { echo 'class="mdc-list-item--activated"'; } ?> href="https://www.storecore.io/" hreflang="en-GB" role="menuitem" title="StoreCore"><i class="material-icons" aria-hidden="true">home</i> <span>Home</span></a></li>
            <li role="presentation"><a <?php if ($uri === '/plans-and-pricing') { echo 'class="mdc-list-item--activated"'; } ?> href="https://www.storecore.io/plans-and-pricing" itemprop="acquireLicensePage" role="menuitem" title="Plans and pricing"><i class="material-icons" aria-hidden="true">view_list</i> <span>Plans and pricing</span></a></li>
          </ul>
          <hr>
          <ul role="group">
            <li role="presentation"><a <?php if ($uri === '/knowledge-base/user-guides/') { echo 'class="mdc-list-item--activated"'; } ?> href="/knowledge-base/user-guides/" role="menuitem" title="User guides"> <i class="material-icons" aria-hidden="true">local_library</i> <span>User guides</span></a></li>
            <li role="presentation"><a <?php if ($uri === '/knowledge-base/design-guides/') { echo 'class="mdc-list-item--activated"'; } ?> href="/knowledge-base/design-guides/" role="menuitem" title="Design guides"><i class="material-icons" aria-hidden="true">style</i> <span>Design guides</span></a></li>
            <li role="presentation"><a <?php if ($uri === '/knowledge-base/developer-guides/') { echo 'class="mdc-list-item--activated"'; } ?>  href="/knowledge-base/developer-guides/" role="menuitem" title="Developer guides"><i class="material-icons" aria-hidden="true">build</i> <span>Developer guides</span></a></li>
          </ul>
          <hr>
          <ul role="group">
            <li role="presentation"><a <?php if ($uri === '/sitemap') { echo 'class="mdc-list-item--activated"'; } ?> href="https://www.storecore.io/sitemap" rel="search" role="menuitem" title="Sitemap"><i class="material-icons" aria-hidden="true">map</i> <span>Sitemap</span></a></li>
            <li role="presentation"><a href="https://gitlab.com/storecore/storecore/-/issues" rel="nofollow noreferrer" role="menuitem" title="StoreCore issues on GitLab"><i class="material-icons" aria-hidden="true">bug_report</i> <span>Report a bug</span></a></li>
            <li role="presentation"><a href="https://storecore.freshdesk.com/support/home" rel="nofollow noreferrer" role="menuitem" title="StoreCore Support"><i class="material-icons" aria-hidden="true">contact_support</i> <span>Contact support</span></a></li>
          </ul>
          <hr>
          <ul role="group">
            <li role="presentation"><a <?php if ($uri === '/licenses') { echo 'class="mdc-list-item--activated"'; } ?> href="https://www.storecore.io/licenses" itemprop="license" rel="license" role="menuitem" title="Licenses"><i class="material-icons" aria-hidden="true">gavel</i> <span>Licenses</span></a></li>
            <li role="presentation"><a <?php if ($uri === '/privacy-policy') { echo 'class="mdc-list-item--activated"'; } ?> href="/privacy-policy" role="menuitem" title="Privacy policy"><i class="material-icons" aria-hidden="true">security</i> <span>Privacy policy</span></a></li>
          </ul>
        </nav>
      </div>
    </amp-sidebar>
    <header amp-fx="float-in-top" class="mdc-top-app-bar mdc-top-app-bar--fixed" itemscope itemtype="https://schema.org/WPHeader">
      <div class="mdc-top-app-bar__row">
        <section class="mdc-top-app-bar__section mdc-top-app-bar__section--align-start">
          <button class="material-icons mdc-top-app-bar__navigation-icon mdc-icon-button" on="tap:sidebar">menu</button>
          <span class="mdc-top-app-bar__title"><a href="https://www.storecore.io/" title="StoreCore">StoreCore</a></span>
        </section>
        <section class="mdc-top-app-bar__section mdc-top-app-bar__section--align-end" role="toolbar">
          <a aria-label="Help" class="material-icons mdc-top-app-bar__action-item mdc-icon-button" href="https://www.storecore.io/knowledge-base/" rel="help" title="Support">help_outline</a>
        </section>
      </div>
    </header>
    <main class="mdc-top-app-bar--fixed-adjust">
