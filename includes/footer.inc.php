    </main>

    <footer itemscope itemtype="https://schema.org/WPFooter" style="content-visibility:auto">
      <div class="footer__left-section">
        <div class="footer__logo" itemprop="copyrightNotice"><a href="https://www.storecore.io/" title="StoreCore">StoreCore</a>™ © 2014–<?= date('Y') ?></div>
        <ul role="menubar">
          <li role="presentation"><a href="/knowledge-base/" rel="help" role="menuitem" title="Knowledge Base (KB)">Help</a></li>
          <li role="presentation"><a href="https://gitlab.com/storecore/storecore/-/issues" rel="nofollow noopener" role="menuitem" title="StoreCore issues on GitLab">Issue tracker</a></li>
          <li role="presentation"><a href="/contact" role="menuitem" title="Contact us">Contact us</a></li>
          <li role="presentation"><a href="/privacy-policy" hreflang="en-GB" role="menuitem" title="Privacy policy">Privacy</a></li>
        </ul>
        <div><p>We use cookies and Google services to understand how you use our site and to improve your experience.  By continuing to use our site, you accept our <a href="/privacy-policy" hreflang="en-GB" style="color:inherit" title="Privacy policy">use of cookies</a> and <a href="/privacy-policy" hreflang="en-GB" style="color:inherit" title="Privacy policy">privacy policy</a>.</p></div>
      </div>
      <div class="footer__right-section" itemscope itemtype="https://schema.org/Organization">
        <a href="https://www.storecore.io/" itemprop="url" title="StoreCore">
          <amp-img alt="StoreCore" height="72" itemprop="logo" layout="fixed" src="/images/StoreCore-icon-144x144.png" srcset="/images/StoreCore-icon-144x144.webp 144w, /images/StoreCore-icon-216x216.webp 216w" width="72"></amp-img>
        </a>
      </div>
    </footer>

    <amp-analytics type="googleanalytics" config="https://www.storecore.io/public/json/ga4.json" data-credentials="include">
    <script type="application/json">
    {
      "vars": {
        "GA4_MEASUREMENT_ID": "G-9ZNT662JY5",
        "GA4_ENDPOINT_HOSTNAME": "www.google-analytics.com",
        "DEFAULT_PAGEVIEW_ENABLED": true,
        "GOOGLE_CONSENT_ENABLED": false,
        "WEBVITALS_TRACKING": true,
        "PERFORMANCE_TIMING_TRACKING": true,
        "SEND_DOUBLECLICK_BEACON": false
      }
    }
    </script>
    </amp-analytics>

  </body>
</html>
