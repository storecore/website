<?php
ob_start('ob_gzhandler');
http_response_code(500);
header('Allow: GET');
header('Cache-Control: no-cache, no-store, must-revalidate');
header('Content-Type: text/html; charset=utf-8');
header('X-Powered-By: StoreCore');
header('X-Robots-Tag: noindex', true);
require '500.html';
