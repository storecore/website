# Change Log

All notable changes to [StoreCore.io](https://www.storecore.io/) will be
documented in this file, in reverse chronological order by release.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2024-07-19]

### Added

- Add [content-visibility: auto](https://web.dev/articles/content-visibility)

### Changed

- Update robots.txt
- Add redirect for change log

## [2024-07-08]

### Changed

- Update [internationalization developer guide](https://www.storecore.io/knowledge-base/developer-guides/internationalization-and-localization)
- Add homepage link to updated developer guide

## [2024-06-13]

### Security

- Set `410 Gone` response on all URLs starting with ?f=
- Exclude URLs in [robots.txt](https://www.storecore.io/robots.txt)

## [2024-05-31]

### Changed

- Rewrite deprecated PHP features

## [2024-05-09]

### Added

- Add change log

### Security

- Update `Expires` date in [security.txt](https://www.storecore.io/.well-known/security.txt)
