<?php
ob_start('ob_gzhandler');
header('Cache-Control: public');
header('Content-Type: text/html; charset=utf-8');
header('X-Powered-By: StoreCore');
?>
<!doctype html>
<html dir="ltr" lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
  <meta name="robots" content="index,follow">
  <title>💚 Thanks!</title>
  <link rel="canonical" href="https://www.storecore.io/thanks">
  <style>
    @font-face {
      font-family: Roboto;
      font-style: normal;
      font-weight: 300;
      src: local('Roboto Light'), local('Roboto-Light'),
           url('/assets/woff2/Roboto-Light.woff2') format('woff2'),
           url('/assets/ttf/Roboto-Light.ttf') format('truetype');
    }

    @font-face {
      font-family: Roboto;
      font-style: normal;
      font-weight: 900;
      src: local('Roboto Black'), local('Roboto-Black'),
           url('/assets/woff2/Roboto-Black.woff2') format('woff2'),
           url('/assets/ttf/Roboto-Black.ttf') format('truetype');
    }

    @font-face {
      font-family: 'Roboto Condensed';
      font-style: normal;
      font-weight: 300;
      src: local('Roboto Condensed Light'), local('RobotoCondensed-Light'),
           url('/assets/woff2/RobotoCondensed-Light.woff2') format('woff2'),
           url('/assets/ttf/RobotoCondensed-Light.ttf') format('truetype');
    }

    @font-face {
      font-family: 'Roboto Condensed';
      font-style: normal;
      font-weight: 400;
      src: local('Roboto Condensed'), local('RobotoCondensed-Regular'),
           url('/assets/woff2/RobotoCondensed-Regular.woff2') format('woff2'),
           url('/assets/ttf/RobotoCondensed-Regular.ttf') format('truetype');
    }

    html,
    body {
      background-color: #000;
      cursor: default;
      box-sizing: border-box;
      -moz-osx-font-smoothing: grayscale;
       -webkit-font-smoothing: antialiased;
      height: 100%;
      margin: 0;
      overflow: hidden;
      padding: 0;
    }

    body {
      display: flex;
      flex-direction: column;
    }
    
    @keyframes credits {
      0% { top: 100% }
      100% { top: -500% }
    }
    
    .wrapper {
      animation: 80s credits linear infinite;
      color: #fff;
      font-family: "Roboto Condensed", "Arial Narrow", sans-serif;
      left: 50%;
      line-height: 1;
      margin-left: -200px;
      max-width: 100%;
      position: absolute;
      text-align: center;
      top: 100%;
      width: 400px;
    }

    h1 {
      margin-bottom: 50px;
      font-family: Roboto, sans-serif;
      font-size: 58px;
      font-weight: 300;
    }

    h1 span {
      font-weight: 900;
    }

    h2 {
      font-size: 42px;
      font-weight: 400;
      margin: 100px 0 1em 0;
      text-transform: uppercase;
    }

    h3 {
      font-size: 18px;
      font-weight: 400;
      margin: 50px 0 5px 0;
      text-transform: uppercase;
    }

    p {
      font-size: 35px;
      font-weight: 300;
      margin: 2px 0 3px 0;
      text-transform: uppercase;
    }

    p small {
      font-size: 18px;
    }

    aside {
      margin: auto;
      padding: 150px 1em 150px 1em;
      text-transform: none;
    }

    aside p {
      color: rgba(255,255,255,.87);
      font-size: 14px;
      line-height: 16px;
      margin: 0 0 1ex 0;
      max-width: 41em;
    }

    a {
      background: inherit;
      color: inherit;
      display: inline-block;
      text-decoration: none;
    }

    .ttn {
      text-transform: none;
    }

    @media (max-width:480px) {
      h2 { font-size: 16pt }
      h3 { font-size: 9pt }
      p { font-size: 10pt; text-transform: none }
      p small { font-size: 7pt}
      aside { margin: auto; max-width: 90% }
      aside p { font-size: 8pt; line-height: 9pt; text-transform: uppercase }
    }
  </style>
</head>
<body>
  <div class='wrapper'>
    <article>
      <h1><span>Store</span>Core</h1>

      <h3>Core Code Committers</h3>
      <p>Ward van der Put <small>lead</small></p>
      <p>Tristan van Bokkem</p>

      <h3>Business Consultants</h3>
      <p>Dirkjan Vis</p>
      <p>Gian Pintus</p>
      <p>Heimen van Duinkerken</p>
      <p>Marcel Landeweerd</p>

      <h3>Database Consultant</h3>
      <p>Ger van Steenderen</p>

      <h3>Programming Consultants</h3>
      <p>Oscar Dunker</p>
      <p>René Weishaupt</p>
      <p>Thomas van den Heuvel</p>
      <p>Wouter de Jong</p>

      <h3>Security Consultants</h3>
      <p>Chris Horeweg</p>
      <p>Dos Moonen</p>

      <h3>StoreCore Logo &amp; Icon Design</h3>
      <p><a href="https://y-graphicdesign.nl/" rel="noopener" title="Y-Graphic Design">Yvonne van der Hop<br>Y-Graphic Design</a></p>

      <h3>Movie Credits CSS</h3>
      <p><a href="https://codepen.io/oknoblich/" rel="noopener noreferrer">Oliver Knoblich</a></p>

      <h3>Expert User Consultants</h3>
      <p>Agnetha van Duuren</p>
      <p>Jan Roelofsen</p>
      <p>Jochem van Mill</p>
      <p>Martine Bakx</p>

      <h3>Many Thanks To</h3>
      <p>Dennis van Orsouw</p>
      <p>Dennis van der Stam</p>
      <p>Eveline Groot Roessink</p>
      <p>Francois Lohuis</p>
      <p>Hans Schipperijn</p>
      <p>Lieuwe Miedema</p>
      <p>Maarten le Blanc</p>
      <p>Margareth Munsters</p>
      <p>Monique Roelofsen</p>
      <p>Our wives and kids</p>
      <p>Peter Santema</p>
      <p>Randau Meijerink</p>
      <p>Richard Theuws</p>
      <p>Rudy van ’t Hoff</p>
      <p>Willem-Johan Bakker</p>

      <h2>StoreCore Team</h2>

      <h3>Chief Executive Officer (CEO)</h3>
      <p>Ward van der Put</p>

      <h3>Chief Marketing Officer (CMO)</h3>
      <p>Patrick Heijmans</p>

      <h3>Founded in 2014 by</h3>
      <p>Ward van der Put</p>
      <p>Tristan van Bokkem</p>

      <h3>GitLab</h3>
      <p class="ttn"><a href="https://gitlab.com/storecore/storecore" rel="noopener noreferrer" title="GitLab">gitlab.com/storecore</a></p>

      <h3>Twitter X</h3>
      <p class="ttn"><a href="https://twitter.com/storecoreio" rel="noopener noreferrer" title="Twitter">@storecoreio</a></p>

      <h3>Facebook</h3>
      <p class="ttn"><a href="https://www.facebook.com/StoreCoreHQ" rel="noopener noreferrer" title="Facebook">facebook.com/StoreCoreHQ</a></p>

      <h3>Web</h3>
      <p class="ttn"><a href="https://www.storecore.io/" title="StoreCore">storecore.io</a></p>

      <aside>
        <p>StoreCore™ E-commerce Framework</p>
        <p>Copyright © 2014–2023 StoreCore™</p>
        <p>
          This program is free software: you can redistribute it and/or modify
          it under the terms of the GNU General Public License as published by
          the Free Software Foundation, either version 3 of the License, or
          (at your option) any later version.
        </p>
        <p>
          This program is distributed in the hope that it will be useful,
          but WITHOUT ANY WARRANTY; without even the implied warranty of
          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
          GNU General Public License for more details.
        </p>
        <p>
          You should have received a copy of the GNU General Public License
          along with this program.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
        </p>
      </aside>

      <h3>Beer &amp; Pizza</h3>
      <p>Bavaria</p>
      <p>Domino’s</p>
    </article>
  </div>
</body>
</html>
