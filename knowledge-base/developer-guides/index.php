<?php require '../../includes/header.inc.php'; ?>
<h1>Developer guides</h1>
<div class="amd-card-collection" style="padding-bottom:24px">

    <div class="mdc-card">
      <a href="/knowledge-base/developer-guides/internationalization-and-localization" title="Internationalization and localization">
        <div class="mdc-card__primary-action">
          <div class="mdc-card__media mdc-card__media--16-9 mdc-card__media--internationalization-and-localization"></div>
          <div><h3 class="mdc-typography mdc-typography--headline6" style="line-height:1.1em">Internationalization and localization</h3></div>
          <div class="mdc-typography mdc-typography--body2">StoreCore supports multiple languages by default. This developer guide describes inter&shy;na&shy;tion&shy;al&shy;iza&shy;tion (<abbr title="internationalization">I18N</abbr>) and local&shy;iza&shy;tion (<abbr title="localization">L10N</abbr>) in the e&#x2011;com&shy;merce frame&shy;work.</div>
        </div>
      </a>
    </div>

    <div class="mdc-card">
      <a href="/knowledge-base/developer-guides/performance-guidelines" title="Performance guidelines">
        <div class="mdc-card__primary-action">
          <div class="mdc-card__media mdc-card__media--16-9 mdc-card__media--performance-guidelines"></div>
          <div><h3 class="mdc-typography mdc-typography--headline6">Performance guidelines</h3></div>
          <div class="mdc-typography mdc-typography--body2">This StoreCore developer guide contains several do’s and don’ts on PHP and MySQL performance.</div>
        </div>
      </a>
    </div>

</div>
<?php require '../../includes/footer.inc.php';
