<?php require '../../includes/header.inc.php'; ?>
<article itemscope itemtype="https://schema.org/TechArticle">
  <nav>
    <ol class="amd-breadcrumb-list" itemscope itemtype="https://schema.org/BreadcrumbList">
      <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a href="https://www.storecore.io/" itemid="https://www.storecore.io/" itemprop="item" itemscope itemtype="https://schema.org/WebSite">
          <span itemprop="name">Home</span>
        </a>
        <meta itemprop="position" content="1" />
      </li>
      <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a href="https://www.storecore.io/knowledge-base/" itemid="https://www.storecore.io/knowledge-base/" itemprop="item" itemscope itemtype="https://schema.org/WebPage">
          <span itemprop="name">Knowledge base</span>
        </a>
        <meta itemprop="position" content="2" />
      </li>
      <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a href="https://www.storecore.io/knowledge-base/developer-guides/" itemid="https://www.storecore.io/knowledge-base/developer-guides/" itemprop="item" itemscope itemtype="https://schema.org/WebPage">
          <span itemprop="name">Developer guides</span>
        </a>
        <meta itemprop="position" content="3" />
      </li>
      <li itemscope itemprop="itemListElement" itemtype="https://schema.org/ListItem">
        <span itemprop="name">Performance guidelines</span>
        <meta itemprop="position" content="4" />
      </li>
    </ol>
  </nav>

  <script type="application/ld+json">
  {
    "@context": "https://schema.org/",
    "@type": "TechArticle",
    "author": {
      "@type": "Person",
      "givenName": "Ward",
      "familyName": "van der Put",
      "name": "Ward van der Put"
    },
    "headline": "Performance guidelines",
    "description": "Performance is one of the StoreCore design principles. This StoreCore developer guide contains several do’s and don’ts on PHP and MySQL performance.",
    "mainEntityOfPage": "https://www.storecore.io/knowledge-base/developer-guides/performance-guidelines",
    "image": [
      "https://www.storecore.io/images/performance-guidelines-1200x1200.jpg",
      "https://www.storecore.io/images/performance-guidelines-1200x900.jpg",
      "https://www.storecore.io/images/performance-guidelines-1200x675.jpg"
    ],
    "datePublished": "<?php echo date(DATE_ATOM, filectime(__FILE__)) ?>",
    "dateModified": "<?php echo date(DATE_ATOM, filemtime(__FILE__)) ?>",
    "publisher": {
      "@type": "Organization",
      "name": "StoreCore",
      "alternateName": "StoreCore.io",
      "url": "https://www.storecore.io/",
      "email": "info@storecore.org",
      "logo": {
        "@type": "ImageObject",
        "url": "https://www.storecore.io/images/StoreCore-logo-225x55.png",
        "width": 225,
        "height": 55
      }
    }
  }
  </script>

  <h1 itemprop="name">Performance guidelines</h1>
  <p itemprop="author" itemscope itemtype="https://schema.org/Person">by <span itemprop="name">Ward van der Put</span></p>
  <p itemprop="description">Performance is one of the leading StoreCore™ design principles.
    This StoreCore developer guide contains several do’s and don’ts on PHP and MySQL performance.</p>
  <p>This documentation is a work in progress.
    It describes prerelease software, and is subject to change.
    All code is released as free and open-source software (<abbr title="free and open-source software">FOSS</abbr>) under the <a href="https://www.gnu.org/licenses/gpl.html" rel="nofollow noreferrer">GNU General Public License</a>.</p>

  <section id="do-your-own-math">
    <h2>Do your own math</h2>
    <p>Recalculating a fixed value on the server is inefficient.
      Calculate the fixed value once yourself and use the result.
      Add a comment if you need to clarify the value.</p>

<h5><span class="material-icons" role="presentation" style="color:#d32f2f">thumb_down</span> Incorrect:</h5>
<pre><code>setcookie('language', $lang, time() + 60 * 60 * 24 * 30, '/');</code></pre>

<h5><span class="material-icons" role="presentation" style="color:#689f38">thumb_up</span> Correct:</h5>
<pre><code><span style="color:#000"><span style="color:#00b">setcookie</span><span style="color:#070">(</span><span style="color:#d00">'language'</span><span style="color:#070">, </span><span style="color:#00b">$lang</span><span style="color:#070">, </span><span style="color:#00b">time</span><span style="color:#070">() + </span><span style="color:#00b">2592000</span><span style="color:#070">, </span><span style="color:#d00">'/'</span><span style="color:#070">);</span></span></code></pre>

<h5><span class="material-icons" role="presentation" style="color:#689f38">thumb_up</span> Recommended:</h5>
<pre><code><span style="color:#000"><span style="color:#ff8000">// Cookie expires in 60 seconds * 60 minutes * 24 hours * 30 days = 2592000 seconds</span>
<span style="color:#00b">setcookie</span><span style="color:#070">(</span><span style="color:#d00">'language'</span><span style="color:#070">, </span><span style="color:#00b">$lang</span><span style="color:#070">, </span><span style="color:#00b">time</span><span style="color:#070">() + </span><span style="color:#00b">2592000</span><span style="color:#070">, </span><span style="color:#d00">'/'</span><span style="color:#070">);</span></span></code></pre>

  </section>

  <section>
    <h2>Don’t use naive getters and setters</h2>
    <p><dfn>Getters</dfn> and <dfn>setters</dfn> are methods that are used to
      get (read) and set (write) the value of an object's property. 
      In object-oriented programming (<abbr title="object-oriented programming">OOP</abbr>),
      getters and setters are often used to control access to <code>private</code>
      or <code>protected</code> properties.</p>

    <p>A <dfn>naive getter</dfn> is a getter (or accessor) that simply returns
      the value of the property.  A naive setter is a setter (or mutator) that
      simply sets the value of the property.  Here is an example of a naive
      getter in PHP:</p>

<pre><code><span style="color:#000"><span style="color:#070">public&nbsp;function&nbsp;</span><span style="color:#00b">getName</span><span style="color:#070">():&nbsp;</span><span style="color:#00b">string
</span><span style="color:#070">{
&nbsp;&nbsp;&nbsp;&nbsp;return&nbsp;</span><span style="color:#00b">$this</span><span style="color:#070">-&gt;</span><span style="color:#00b">name</span><span style="color:#070">;
}</span></span></code></pre>

    <p>This getter simply returns the value of the <code>$name</code> class property.
      A similar naive setter for the same property would be something like this:</p>

<pre><code><span style="color:#000"><span style="color:#070">public&nbsp;function&nbsp;</span><span style="color:#00b">setName</span><span style="color:#070">(</span><span style="color:#00b">string&nbsp;$name</span><span style="color:#070">):&nbsp;</span><span style="color:#00b">void
</span><span style="color:#070">{
&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="color:#00b">$this</span><span style="color:#070">-&gt;</span><span style="color:#00b">name&nbsp;</span><span style="color:#070">=&nbsp;</span><span style="color:#00b">$name</span><span style="color:#070">;
}</span></span></code></pre>

    <p>This setter is naive because it does not do any validation or checking
      to ensure that the value is valid.  If <a href="https://www.php.net/manual/en/language.types.declarations.php#language.types.declarations.strict">strict typing</a>
      is enforced in PHP, the method only allows string input, but this string
      may be empty.</p>

    <p>In general, you should avoid using naive getters and naive setters in
      object-oriented PHP. They merely add code that basically does nothing.</p>

<h5><span class="material-icons" role="presentation" style="color:#d32f2f">thumb_down</span> Not recommended:</h5>
<pre><code><span style="color:#000"><span style="color:#070">class&nbsp;</span><span style="color:#00b">Person
</span><span style="color:#070">{
&nbsp;&nbsp;&nbsp;&nbsp;private&nbsp;</span><span style="color:#00b">string&nbsp;$firstName</span><span style="color:#070">;
&nbsp;&nbsp;&nbsp;&nbsp;private&nbsp;</span><span style="color:#00b">string&nbsp;$lastName</span><span style="color:#070">;

&nbsp;&nbsp;&nbsp;&nbsp;public&nbsp;function&nbsp;</span><span style="color:#00b">setFirstName</span><span style="color:#070">(</span><span style="color:#00b">string&nbsp;$first_name</span><span style="color:#070">):&nbsp;</span><span style="color:#00b">void
&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="color:#070">{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="color:#00b">$this</span><span style="color:#070">-&gt;</span><span style="color:#00b">firstName&nbsp;</span><span style="color:#070">=&nbsp;</span><span style="color:#00b">$first_name</span><span style="color:#070">;
&nbsp;&nbsp;&nbsp;&nbsp;}

&nbsp;&nbsp;&nbsp;&nbsp;public&nbsp;function&nbsp;</span><span style="color:#00b">getFirstName</span><span style="color:#070">():&nbsp;</span><span style="color:#00b">string
&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="color:#070">{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return&nbsp;</span><span style="color:#00b">$this</span><span style="color:#070">-&gt;</span><span style="color:#00b">firstName</span><span style="color:#070">;
&nbsp;&nbsp;&nbsp;&nbsp;}

&nbsp;&nbsp;&nbsp;&nbsp;public&nbsp;function&nbsp;</span><span style="color:#00b">setLastName</span><span style="color:#070">(</span><span style="color:#00b">string&nbsp;$last_name</span><span style="color:#070">):&nbsp;</span><span style="color:#00b">void
&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="color:#070">{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="color:#00b">$this</span><span style="color:#070">-&gt;</span><span style="color:#00b">lastName&nbsp;</span><span style="color:#070">=&nbsp;</span><span style="color:#00b">$last_name</span><span style="color:#070">;
&nbsp;&nbsp;&nbsp;&nbsp;}
&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;public&nbsp;function&nbsp;</span><span style="color:#00b">getLastName</span><span style="color:#070">():&nbsp;</span><span style="color:#00b">string
&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="color:#070">{
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return&nbsp;</span><span style="color:#00b">$this</span><span style="color:#070">-&gt;</span><span style="color:#00b">lastName</span><span style="color:#070">;
&nbsp;&nbsp;&nbsp;&nbsp;}
}</span></span></code></pre>

<h5><span class="material-icons" role="presentation" style="color:#689f38">thumb_up</span> Recommended:</h5>
<pre><code><span style="color:#000"><span style="color:#070">class&nbsp;</span><span style="color:#00b">Person
</span><span style="color:#070">{
&nbsp;&nbsp;&nbsp;&nbsp;public&nbsp;</span><span style="color:#00b">string&nbsp;$firstName</span><span style="color:#070">;
&nbsp;&nbsp;&nbsp;&nbsp;public&nbsp;</span><span style="color:#00b">string&nbsp;$lastName</span><span style="color:#070">;
}</span></span></code></pre>

    <p>Here are some additional things to keep in mind when using getters and setters:</p>
    <ul>
      <li>Getters and setters should be used to encapsulate <code>private</code> or <code>protected</code> properties.</li>
      <li>Setters should be used to perform validation and other checks on the value of a property before it is set.</li>
      <li>Getters should be used to return the value of a property in a consistent format.</li>
    </ul>
  </section>

  <section>
    <h2>Order database table columns for performance</h2>
    <p>In some databases, it is more efficient to order the columns in a specific manner because of the way the disk access is performed.
      The optimal order of columns in a MySQL or MariaDB table that uses the InnoDB storage engine is:</p>

    <ul>
      <li>primary key</li>
      <li>combined primary keys as defined in the <code>KEY</code> order</li>
      <li>foreign keys used in <code>JOIN</code> queries</li>
      <li>columns with an <code>INDEX</code> used in <code>WHERE</code> conditions or <code>ORDER BY</code> statements</li>
      <li>others columns used in <code>WHERE</code> conditions</li>
      <li>others columns used in <code>ORDER BY</code> statements</li>
      <li><code>VARCHAR</code> columns with a variable length</li>
      <li>large <code>TEXT</code> and <code>BLOB</code> columns.</li>
    </ul>

    <p>When there are many <code>VARCHAR</code> columns (with variable length) in a MySQL or MariaDB table, the column order MAY affect the performance of queries.
      The less close a column is to the beginning of the row, the more preceding columns the InnoDB storage engine should examine to find out the offset of a given one.
      Columns that are closer to the beginning of the table are therefore selected faster.</p>
  </section>

  <section>
    <h2>Store DateTimes as UTC timestamps</h2>
    <p>Times and dates with times SHOULD be stored in Coordinated Universal Time (<abbr title="Coordinated Universal Time">UTC</abbr>).
      The following examples illustrate this requirement with column definitions in a <code>CREATE TABLE</code> <abbr title="Structured Query Language">SQL</abbr> statement.</p>

<h5><span class="material-icons" role="presentation" style="color:#d32f2f">thumb_down</span> Incorrect:</h5>
<pre><code>`date_created`  DATETIME  NOT NULL</code></pre>

<h5><span class="material-icons" role="presentation" style="color:#689f38">thumb_up</span> Correct:</h5>
<pre><code>`date_created`  DATETIME  NOT NULL  DEFAULT CURRENT_TIMESTAMP</code></pre>

<h5><span class="material-icons" role="presentation" style="color:#d32f2f">thumb_down</span> Incorrect:</h5>
<pre><code>`date_modified`  DATETIME  NOT NULL</code></pre>

<h5><span class="material-icons" role="presentation" style="color:#689f38">thumb_up</span> Correct:</h5>
<pre><code>`date_modified`  DATETIME  NOT NULL  ON UPDATE CURRENT_TIMESTAMP</code></pre>

    <p>When there are two timestamps in the same database table, the logical
      thing to do is setting the creation date <code>date_created</code> to
      <code>DEFAULT CURRENT_<wbr>TIMESTAMP</code> for the initial <code>INSERT</code>
      query and the modification date <code>date_<wbr>modified</code> to
      <code>ON UPDATE CURRENT_<wbr>TIMESTAMP</code> for all subsequent
      <code>UPDATE</code> queries:

<h5><span class="material-icons" role="presentation" style="color:#d32f2f">thumb_down</span> Not recommended:</h5>
<pre><code>`date_created`   DATETIME  NOT NULL  DEFAULT CURRENT_TIMESTAMP,
`date_modified`  DATETIME  NOT NULL  DEFAULT '0000-00-00 00:00:00'  ON UPDATE CURRENT_TIMESTAMP</code></pre>

  <p>This, however, only works in MySQL 5.6+.  Older versions of MySQL will
    report an error: <q>Incorrect table definition; there can be only one
    <code>TIMESTAMP</code> column with <code>CURRENT_<wbr>TIMESTAMP</code> in
    <code>DEFAULT</code> or <code>ON UPDATE</code> clause.</q></p>

  <p>The workaround currently implemented in Store&shy;Core is to set the
    <code>DEFAULT</code> value for the initial <code>INSERT</code> timestamp
    to <code>'0000-00-00 00:00:00'</code> and only use the
    <code>CURRENT_<wbr>TIMESTAMP</code> for a subsequent <code>ON UPDATE</code>:

<h5><span class="material-icons" role="presentation" style="color:#689f38">thumb_up</span> Recommended:</h5>
<pre><code>`date_created`   DATETIME  NOT NULL  DEFAULT '0000-00-00 00:00:00',
`date_modified`  DATETIME  NOT NULL  DEFAULT CURRENT_TIMESTAMP  ON UPDATE CURRENT_TIMESTAMP</code></pre>

  </section>

  <section>
    <h2>Don’t cast database integers to <abbr title="Structured Query Language">SQL</abbr> strings</h2>
    <p>String equality comparisons are much more expensive database operations than integer compares.
      If a database value is an integer, it MUST NOT be treated as a numeric string.
      This holds especially true for primary keys and foreign keys.</p>

<h5><span class="material-icons" role="presentation" style="color:#d32f2f">thumb_down</span> Incorrect:</h5>
<pre><code>$sql = "
    UPDATE sc_addresses
       SET customer_id = '" . (int) $customer_id . "'
     WHERE address_id = '" . (int) $address_id . "'";</code></pre>

<h5><span class="material-icons" role="presentation" style="color:#689f38">thumb_up</span> Correct:</h5>
<pre><code>$sql = '
    UPDATE sc_addresses
       SET customer_id = ' . (int) $customer_id . '
     WHERE address_id = ' . (int) $address_id;</code></pre>

    <p>The first PHP statement creates an <abbr title="Structured Query Language">SQL</abbr>
      expression with the numeric strings <code>'54321'</code> and <code>'67890'</code>,
      and the second statement an expression with the true integer values <code>54321</code>
      and <code>67890</code>:</p>

<h5><span class="material-icons" role="presentation" style="color:#d32f2f">thumb_down</span> Incorrect:</h5>
<pre><code>UPDATE sc_addresses
   SET customer_id = '54321'
 WHERE address_id  = '67890';</code></pre>

<h5><span class="material-icons" role="presentation" style="color:#689f38">thumb_up</span> Correct:</h5>
<pre><code>UPDATE sc_addresses
   SET customer_id = 54321
 WHERE address_id  = 67890;</code></pre>

  </section>

  <section>
    <h2>Don’t close and immediately re-open PHP tags</h2>
    <p>A common mistake in PHP templates and <abbr title="Model-View-Controller">MVC</abbr> views is closing and immediately re-opening PHP-tags.</p>

<h5><span class="material-icons" role="presentation" style="color:#d32f2f">thumb_down</span> Incorrect:</h5>
<pre><code>&lt;?php echo $header; ?&gt;&lt;?php echo $menu; ?&gt;</code></pre>

<h5><span class="material-icons" role="presentation" style="color:#d32f2f">thumb_down</span> Incorrect:</h5>
<pre><code>&lt;?php echo $header; ?&gt;
&lt;?php echo $menu; ?&gt;</code></pre>

<h5><span class="material-icons" role="presentation" style="color:#689f38">thumb_up</span> Correct:</h5>
<pre><code>&lt;?php
echo $header;
echo $menu;
?&gt;</code></pre>

<h5><span class="material-icons" role="presentation" style="color:#689f38">thumb_up</span> Correct:</h5>
<pre><code>&lt;?php
echo $header, $menu;
?&gt;</code></pre>

<h5><span class="material-icons" role="presentation" style="color:#689f38">thumb_up</span> Correct:</h5>
<pre><code>&lt;?php echo $header, $menu; ?&gt;</code></pre>

  </section>

  <section>
    <h2>Return results early</h2>
    <p>Once the result of a PHP method or function has been established, it SHOULD
      be returned.  The examples below demonstrate this may save memory and
      computations.</p>

<h5><span class="material-icons" role="presentation" style="color:#d32f2f">thumb_down</span> Incorrect:</h5>
<pre><code>public function hasDownload()
{
    $download = false;

    foreach ($this->getProducts() as $product) {
        if ($product['download']) {
            $download = true;
            break;
        }
    }

    return $download;
}</code></pre>

<h5><span class="material-icons" role="presentation" style="color:#689f38">thumb_up</span> Correct:</h5>
<pre><code><span style="color:#070">public function</span> <span style="color:#00B">hasDownload</span><span style="color:#070">()
{
    foreach (</span><span style="color:#00B">$this</span><span style="color:#070">-&gt;</span><span style="color:#00B">getProducts</span><span style="color:#070">() as </span><span style="color:#00B">$product</span><span style="color:#070">) {
        if (</span><span style="color:#00B">$product</span><span style="color:#070">[</span><span style="color:#D00">'download'</span><span style="color:#070">]) {
            return </span><span style="color:#00B">true</span><span style="color:#070">;
        }
    }
    return</span> <span style="color:#00B">false</span><span style="color:#070">;
}</span></code></pre>

    <p>Adding a temporary variable and two lines of code for a simple <code>true</code>
      or <code>false</code> does not really make sense.  First breaking from an
      <code>if</code> nested in a <code>foreach</code> loop doesn’t make much sense
      either if you can just as well <code>return</code> the result immediately.</p>
    
    <p>One of the reasons to return results at the end of a PHP method, is that no
      <code>return</code> can ever be overlooked.  With a single <code>return</code>
      at the end, a method has a single point of exit, much like the method signature
      with the function parameters serves as a single point of entry.</p>

    <p>However, if there is indeed a risk that a <code>return</code> might be
      overlooked, then the method may be too long.  The <code>function</code> then
      probably can be split in multiple functions, with each function handling one of
      the <code>return</code> cases.</p>

    <p>With a type declaration for the <code>return</code> value, introduced in
      PHP&nbsp;7, there is no need to search for possible <code>return</code> values
      inside a method.  The <code>$download</code> variable in the first, incorrect
      example illustrates that introducing an extra variable for the result is 
      no guarantee for clean code: the <code>return $download</code> incorrectly
      suggests that a download will be returned.</p>

<h5><span class="material-icons" role="presentation" style="color:#689f38">thumb_up</span> Recommended:</h5>
<pre><code><span style="color:#070">public function </span><span style="color:#00B">hasDownload</span><span style="color:#070">():</span> <span style="color:#00B">bool
</span><span style="color:#070">{
    foreach (</span><span style="color:#00B">$this</span><span style="color:#070">-&gt;</span><span style="color:#00B">getProducts</span><span style="color:#070">() as </span><span style="color:#00B">$product</span><span style="color:#070">) {
        if (</span><span style="color:#00B">$product</span><span style="color:#070">[</span><span style="color:#d00">'download'</span><span style="color:#070">]) return </span><span style="color:#00B">true</span><span style="color:#070">;
    }
    return</span> <span style="color:#00B">false</span><span style="color:#070">;
}</span></code></pre>

  </section>
  <aside>
    <p><small>Except as otherwise noted, the content of this page is licensed under the <a href="https://creativecommons.org/licenses/by/4.0/" rel="nofollow noreferrer">Creative Commons Attribution 4.0 License</a>, and code samples are licensed under the <a href="https://www.gnu.org/licenses/gpl.html" rel="nofollow noreferrer">GNU General Public License version 3 (GPLv3)</a>.</small></p>
  </aside>
</article>
<?php require '../../includes/footer.inc.php';
