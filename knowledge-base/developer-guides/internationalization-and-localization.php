<?php require '../../includes/header.inc.php'; ?>
<article lang="en-US">
  <nav>
    <ol class="amd-breadcrumb-list" itemscope itemtype="https://schema.org/BreadcrumbList">
      <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a href="https://www.storecore.io/" itemid="https://www.storecore.io/" itemprop="item" itemscope itemtype="https://schema.org/WebSite">
          <span itemprop="name">Home</span>
        </a>
        <meta itemprop="position" content="1" />
      </li>
      <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a href="https://www.storecore.io/knowledge-base/" itemid="https://www.storecore.io/knowledge-base/" itemprop="item" itemscope itemtype="https://schema.org/WebPage">
          <span itemprop="name">Knowledge base</span>
        </a>
        <meta itemprop="position" content="2" />
      </li>
      <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a href="https://www.storecore.io/knowledge-base/developer-guides/" itemid="https://www.storecore.io/knowledge-base/developer-guides/" itemprop="item" itemscope itemtype="https://schema.org/WebPage">
          <span itemprop="name">Developer guides</span>
        </a>
        <meta itemprop="position" content="3" />
      </li>
      <li itemscope itemprop="itemListElement" itemtype="https://schema.org/ListItem">
        <span itemprop="name">Internationalization and localization</span>
        <meta itemprop="position" content="4" />
      </li>
    </ol>
  </nav>

  <script type="application/ld+json">
  {
    "@context": "https://schema.org",
    "@type": "Article",
    "author": {
      "@type": "Person",
      "givenName": "Ward",
      "familyName": "van der Put",
      "name": "Ward van der Put"
    },
    "headline": "Internationalization and localization",
    "description": "StoreCore supports multiple webshops in multiple languages.  This guide describes how internationalization (I18N) and localization (L10N) are implemented.",
    "mainEntityOfPage": "https://www.storecore.io/knowledge-base/developer-guides/internationalization-and-localization",
    "image": [
      "https://www.storecore.io/images/internationalization-and-localization-1200x1200.jpg",
      "https://www.storecore.io/images/internationalization-and-localization-1200x900.jpg",
      "https://www.storecore.io/images/internationalization-and-localization-1200x675.jpg"
    ],
    "datePublished": "2020-05-01T18:25:53+02:00",
    "dateModified": "<?php echo date(DATE_ATOM, filemtime(__FILE__)) ?>",
    "publisher": {
      "@type": "Organization",
      "name": "StoreCore",
      "alternateName": "StoreCore.io",
      "url": "https://www.storecore.io/",
      "email": "info@storecore.org",
      "logo": {
        "@type": "ImageObject",
        "url": "https://www.storecore.io/images/StoreCore-logo-225x55.png",
        "width": 225,
        "height": 55
      }
    }
  }
  </script>

  <h1>Inter&shy;na&shy;tion&shy;al&shy;iza&shy;tion and local&shy;iza&shy;tion</h1>
  <p>by <span lang="nl-NL"><a href="https://gitlab.com/wardvanderput" rel="author external">Ward van der Put</a></span></p>
  <p>StoreCore is a multi-store ecommerce frame&shy;work that supports multiple
    webshops in multiple languages by default.  This developer guide describes
    how internationalization (<abbr title="internationalization">I18N</abbr>)
    and localization (<abbr title="localization">L10N</abbr>) are implemented
    throughout the framework.</p>
  <aside>
    <p>This documentation is a work in progress.  It describes prerelease
      software, and is subject to change.  All StoreCore code is released as free
      and open-source software (<abbr title="free and open-source software">FOSS</abbr>)
      under the <a href="https://gitlab.com/storecore/storecore/-/blob/develop/LICENSE.html"
      rel="nofollow noreferrer">GNU General Public License</a>.</p>
  </aside>

  <section>
    <h2>Limitations of <abbr title="model-view-controller-language">MVC-L</abbr></h2>
    <p>From the outset we decided multilingual support of <a
      href="https://en.m.wikipedia.org/wiki/Languages_of_Europe"
      rel="noreferrer" title="Languages_of_Europe">European languages</a>
      SHOULD be a key feature of StoreCore as an open-source ecommerce
      community operating from Europe.  Support of multiple languages would no
      longer be an option, but a MUST.  For companies operating in bilingual
      and multilingual European countries like Belgium, Luxembourg, and
      Switzerland this may of course be an critical key feature too.</p>
    <p>Early experiments for StoreCore were based on an OpenCart fork called
      <a href="https://github.com/wardvanderput/SumoStore"
      rel="nofollow noreferrer" title="SumoStore on GitHub">SumoStore</a>
      (in 2014) and on a fork of <a href="https://github.com/opencart/opencart"
      rel="nofollow noreferrer" title="OpenCart on GitHub">OpenCart</a> itself
      (in 2015).  Both OpenCart and SumoStore use an application design pattern
      known as <dfn>MVC-L</dfn> or <dfn>MVC+L</dfn>: traditional
      Model-View-Controller (<abbr title="model-view-controller">MVC</abbr>)
      with an extra Language (L) dimension.  Ultimately we dropped this
      <abbr title="model-view-controller-language">MVC-L</abbr> architecture
      however, for several reasons.</p>
    <p>The basic <abbr title="model-view-controller-language">MVC-L</abbr>
      (model-view-controller-language) application structure adds severe
      limitations to performance, maintenance, and scalability.  For example,
      a single language adds over 350 files in about 40 directories to an
      OpenCart install.  If the OpenCart <abbr
      title="model-view-controller-language">MVC-L</abbr> implementation is
      expanded to four or even more languages, file management becomes a
      dreadful task.</p>
    <p>There are performance side-effects if a single <abbr
      title="model-view-controller">MVC</abbr> view consists of not only one
      template file, but also several language files for all supported
      languages.</p>
    <p>Furthermore, <em>consistency</em> within one language is difficult to
      maintain if terms are spread out over dozens of language files.  For
      example, if the store manager wants to change <em>shopping cart</em> to
      <em>shopping basket</em>, a developer will have go over several files.
      A more centralized approach with an end-user interface for editing seems
      a much wiser choice.</p>
  </section>

  <section>
    <h2>Translation memory (<abbr title="translation memory">TM</abbr>)</h2>
    <p>StoreCore uses a <a href="https://en.m.wikipedia.org/wiki/Translation_memory"
      rel="noreferrer">translation memory (<abbr title="translation memory">TM</abbr>)</a>
      to handle and maintain all language strings.  The translation memory
      database table is defined in the main <abbr title="Structured Query Language">SQL</abbr>
      <abbr title="data definition language">DDL</abbr> (data definition
      language) file <code>core-mysql.sql</code> for MySQL and MariaDB.  The translations
      are included in a separate <abbr title="Structured Query Language">SQL</abbr>
      <abbr title="data manipulation language">DML</abbr> (data manipulation
      language) data file called <code>i18n-dml.sql</code>.</p>
    
<p>The StoreCore translation memory (<abbr title="translation memory">TM</abbr>)
  has the following database table structure for MySQL and MariaDB:</p>

<pre><code>CREATE TABLE IF NOT EXISTS `sc_translation_memory` (
  `translation_id`   VARCHAR(63)  CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL,
  `language_id`      VARCHAR(13)  CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL  DEFAULT 'en-GB',
  `admin_only_flag`  BIT(1)       NOT NULL  DEFAULT b'0',
  `date_modified`    TIMESTAMP    NOT NULL  DEFAULT CURRENT_TIMESTAMP  ON UPDATE CURRENT_TIMESTAMP,
  `translation`      TEXT         NULL,
  PRIMARY KEY (`translation_id`, `language_id`),
  FOREIGN KEY `fk_translation_memory_languages` (`language_id`) REFERENCES `sc_languages` (`language_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;</code></pre>

<p>The foreign key (<abbr title="foreign key">FK</abbr>) <code>language_id</code>
  in the <code>sc_<wbr>translation_<wbr>memory</code> database table points to
  the primary key (<abbr title="primary key">PK</abbr>) <code>language_id</code>
  in the <code>sc_languages</code> table for all supported languages:</p>

<pre><code>CREATE TABLE IF NOT EXISTS `sc_languages` (
  `language_id`   VARCHAR(13)       CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL,
  `parent_id`     VARCHAR(13)       CHARACTER SET ascii  COLLATE ascii_bin  NOT NULL  DEFAULT 'en-GB',
  `enabled_flag`  BIT(1)            NOT NULL  DEFAULT b'0',
  `sort_order`    TINYINT UNSIGNED  NOT NULL  DEFAULT 0,
  `english_name`  VARCHAR(40)       NOT NULL,
  `local_name`    VARCHAR(40)       NULL  DEFAULT NULL,
  PRIMARY KEY (`language_id`),
  FOREIGN KEY `fk_languages_languages` (`parent_id`) REFERENCES `sc_languages` (`language_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  UNIQUE KEY `uk_english_name` (`english_name`),
  INDEX `ix_languages` (`enabled_flag` DESC, `sort_order` ASC, `local_name` ASC, `english_name` ASC)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4  COLLATE=utf8mb4_unicode_ci;</code></pre>

<p>Database table definitions are included in the 
    <a href-"https://gitlab.com/storecore/storecore/-/blob/develop/src/Database/core-mysql.sql" rel="nofollow noopener noreferrer" title="src/Database/core-mysql.sql on GitLab">core-mysql.sql</a>
      <abbr title="Structured Query Language">SQL</abbr> file.</p>

    <section>
      <h3>Root: core or master languages</h3>
      <p><dfn>Core languages</dfn>, or <dfn>masters</dfn>, are root-level
        language packs.  They SHOULD NOT be deleted.  If the <code>parent_id</code>
        of a language is equal to the <code>en-GB</code>, that language is a
        core language located at the root of the language family tree.</p>

      <p>Currently, the StoreCore core supports four European master languages.
        These are defined in the <code>SUPPORTED_LANGUAGES</code> class constant
        of the <code>LanguagePacks</code> class:</p>
      <ul>
        <li><code>de-DE</code> for German
        <li><code>en-GB</code> for English
        <li><code>fr-FR</code> for French
        <li><code>nl-NL</code> for Dutch.
      </ul>

      <p>If no language match is found, StoreCore always defaults to
        <code>en-GB</code> for British English.  You could therefore say that
        British English &mdash; or European English &mdash; is the master of
        all masters in the StoreCore translation memory.</p>

      <p>Master languages SHOULD NOT be deleted, but they MAY be disabled.  The
        example below illustrates that you should not <code>DELETE</code> a master
        language (in this case <code>de-DE</code> for German), but you can disable
        the language by settings its <code>enabled_flag</code> status code to
        <code>0</code> (zero) or <code>b'0'</code> (binary zero).</p>

<h5><span class="material-icons" role="presentation" style="color:#d32f2f">thumb_down</span> Not recommended:</h5>
<pre><code>DELETE
  FROM sc_languages
 WHERE language_id = 'de-DE'</code></pre>

<h5><span class="material-icons" role="presentation" style="color:#689f38">thumb_up</span> Recommended:</h5>
<pre><code>UPDATE sc_languages
   SET status = 0
 WHERE language_id = 'de-DE'</code></pre>

    </section>

    <section>
      <h3>Tree and branches: secondary languages</h3>
      <p><dfn>Secondary languages</dfn> are derived from the core/master
        languages.  They only contain differences (deltas) with the master
        language.  For example, the “English – United States” or
        <code>en-US</code> language pack only contains the differences between
        American English and British English in its “English – United Kingdom”
        or <code>en-GB</code> master.  This allows for global localization while
        maintaining language consistency and a concise dictionary.</p>
    </section>
  </section>

  <section>
    <h2>Content language negotiation</h2>
    <p>StoreCore uses the <abbr title="Hypertext Transfer Protocol">HTTP</abbr>
      <code>Accept-Language</code> header to determine which content language
      is preferred by visitors, customers, users, and client applications.  The
      current language can be found by supplying an array of supported
      languages to the <code>Language::<wbr>negotiate()</code> method.</p>

<h5>Class synopsis</h5>
<pre><code>Language {
    public string negotiate ( array $supported [, string $default = 'en-GB'] )
}</code></pre>

    <p>The <code>$supported</code> parameter must be an associative array of
      <abbr title="International Organization for Standardization">ISO</abbr>
      language codes that evaluate to <code>true</code>.  For example, if an
      application supports both English and French, the supported languages may
      be defined as:</p>

<pre><code>$supported = array(
    'en-GB' => true,
    'fr-FR' => true,
);</code></pre>

    <p>This data structure allows you to temporarily disable a supported
      language, without fully dropping it:</p>

<pre><code>$supported = array(
    'en-GB' => true,
    'fr-FR' => false,
);</code></pre>

  </section>

  <section id="translation-guidelines">
    <h2>Translation guidelines</h2>

    <section>
      <h3>Language components</h3>
      <p>The translation memory contains seven types of components, divided
        into two groups.  These types are namespaced with an uppercase
        prefix.</p>

      <p>The first group contains basic language constructs in plain text,
        without any formatting:</p>
      <ul>
        <li><code>ADJECTIVE</code> for adjectives</li>
        <li><code>NOUN</code> for nouns and names</li>
        <li><code>VERB</code> for verbs.</li>
      </ul>

      <p>The second group is used in user interfaces and MAY contain
        formatting, usually <abbr title="Hypertext Markup Language 5">HTML5</abbr>
        or <abbr title="Accelerated Mobile Pages">AMP</abbr>
        <abbr title="Hypertext Markup Language">HTML</abbr>:</p>
      <ul>
        <li><code>COMMAND</code> for menu commands and command buttons</li>
        <li><code>ERROR</code> for error messages</li>
        <li><code>HEADING</code> for headings and form labels</li>
        <li><code>TEXT</code> for anything else.</li>
      </ul>
    </section>

    <section>
      <h3>Compound nouns</h3>
      <p>Compound nouns are handled as single nouns.  For example, <em>shopping
        cart</em> is not stored as two terms like <code>NOUN_<wbr>SHOPPING</code>
        plus <code>NOUN_<wbr>CART</code>, but as a single segment like
        <code>NOUN_<wbr>SHOPPING_<wbr>CART</code>.</p>
    </section>

    <section>
      <h3>Names as nouns</h3>
      <p>Names are treated as nouns.  Therefore they contain the default
        <code>NOUN</code> prefix, for example <code>NOUN_PAYPAL</code> for
        <em>PayPal</em> and <code>NOUN_GOOGLE_ANALYTICS</code> for <em>Google
        Analytics</em>.</p>
    </section>

    <section>
      <h3>Verbs to commands</h3>
      <p>Commands, menu commands, and command buttons usually indicate an
        activity.  Therefore commands SHOULD be derived from verbs.  The
        translation memory <abbr title="Structured Query Language">SQL</abbr>
        file contains an example of this business logic.  The general verb
        <em>print</em> in lowercase becomes the command
        <strong>Print…</strong> with an uppercase first letter and three dots
        in user interfaces:</p>

<pre><code>INSERT IGNORE INTO `sc_translation_memory`
    (`translation_id`, `language_id`, `translation`)
  VALUES
    ('VERB_PRINT', 'ca-039', 'imprimir'),
    ('VERB_PRINT', 'de-DE', 'drucken'),
    ('VERB_PRINT', 'en-GB', 'print'),
    ('VERB_PRINT', 'es-ES', 'imprimir'),
    ('VERB_PRINT', 'fr-FR', 'imprimer'),
    ('VERB_PRINT', 'it-IT', 'stampare'),
    ('VERB_PRINT', 'nl-NL', 'printen'),
    ('VERB_PRINT', 'pt-PT', 'imprimir');

INSERT IGNORE INTO `sc_translation_memory`
    (`translation_id`, `language_id`, `translation`)
  VALUES
    ('COMMAND_PRINT', 'ca-039', 'Imprimeix…'),
    ('COMMAND_PRINT', 'de-DE', 'Drucken…'),
    ('COMMAND_PRINT', 'en-GB', 'Print…'),
    ('COMMAND_PRINT', 'es-ES', 'Imprimir…'),
    ('COMMAND_PRINT', 'fr-FR', 'Imprimer…'),
    ('COMMAND_PRINT', 'it-IT', 'Stampa…'),
    ('COMMAND_PRINT', 'nl-NL', 'Printen…'),
    ('COMMAND_PRINT', 'pt-PT', 'Imprimir…');</code></pre>

      <p>In some cases verbs are included in the translation memory for
        reference purposes and consistency.  For example, the English verb
        <em>to&nbsp;print</em> has three common translations in Dutch:
        <span lang="nl-NL">“printen”</span>, <span lang="nl-NL">“afdrukken”</span>, and <span lang="nl-NL">“drukken”</span>.
        The definition <code lang="nl-NL">printen</code> of <code>VERB_PRINT</code>
        in <code>nl-NL</code> thus indicates the preferred translation for
        Dutch (<code>nl-NL</code>) in the Netherlands (<code>NL</code>).</p>
    </section>

    <section>
      <h3>Errors and exceptions</h3>
      <p>Error messages and exception message strings currently are not
        translated when these are intended primarily for developers and server
        administrators.</p>
    </section>
  </section>
  <aside>
    <p><small>Except as otherwise noted, the content of this page is licensed under the <a href="https://creativecommons.org/licenses/by/4.0/" rel="nofollow noreferrer">Creative Commons Attribution&nbsp;4.0 License</a>, and code samples are licensed under the <a href="https://www.gnu.org/licenses/gpl.html" rel="nofollow noreferrer">GNU General Public License version&nbsp;3 (GPLv3)</a>.</small></p>
  </aside>
</article>
<?php require '../../includes/footer.inc.php';
