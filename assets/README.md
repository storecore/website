## Material Icons

Material Icons at Google Fonts:

* https://fonts.google.com/icons

Material Icons Guide at Google Fonts:

* https://developers.google.com/fonts/docs/material_icons

Material Icons Guide at Google Design:

* https://google.github.io/material-design-icons/

Material Icons font repository:

* https://github.com/material-icons/material-icons-font
